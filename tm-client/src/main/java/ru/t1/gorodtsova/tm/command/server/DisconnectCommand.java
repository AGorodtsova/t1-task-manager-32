package ru.t1.gorodtsova.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.command.AbstractCommand;
import ru.t1.gorodtsova.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    private final String DESCRIPTION = "Disconnect from server";

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        try {
            getServiceLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
