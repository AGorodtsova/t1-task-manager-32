package ru.t1.gorodtsova.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.client.ISystemEndpointClient;
import ru.t1.gorodtsova.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.gorodtsova.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.gorodtsova.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.gorodtsova.tm.dto.response.system.ApplicationVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

}
