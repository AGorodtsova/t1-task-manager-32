package ru.t1.gorodtsova.tm.api.client;

import ru.t1.gorodtsova.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {
}
