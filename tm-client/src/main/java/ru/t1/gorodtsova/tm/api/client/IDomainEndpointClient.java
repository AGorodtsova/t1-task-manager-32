package ru.t1.gorodtsova.tm.api.client;

import ru.t1.gorodtsova.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IDomainEndpoint, IEndpointClient {
}
