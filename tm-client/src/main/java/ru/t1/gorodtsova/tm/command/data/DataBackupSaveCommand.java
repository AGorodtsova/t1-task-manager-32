package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.domain.DataBackupLoadRequest;
import ru.t1.gorodtsova.tm.dto.request.domain.DataBackupSaveRequest;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save backup to file";

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest();
        getDomainEndpoint().saveDataBackup(request);
    }

}
