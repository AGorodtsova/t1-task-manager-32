package ru.t1.gorodtsova.tm.api.client;

import ru.t1.gorodtsova.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {
}
