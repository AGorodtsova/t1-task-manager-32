package ru.t1.gorodtsova.tm.api.client;

import ru.t1.gorodtsova.tm.api.endpoint.ITaskEndpoint;

public interface ITaskEndpointClient extends ITaskEndpoint, IEndpointClient {
}
