package ru.t1.gorodtsova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.client.IAuthEndpointClient;
import ru.t1.gorodtsova.tm.api.client.IUserEndpointClient;
import ru.t1.gorodtsova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpointClient getUserEndpointClient() {
        return getServiceLocator().getUserEndpointClient();
    }

    @NotNull
    protected IAuthEndpointClient getAuthEndpointClient() {
        return getServiceLocator().getAuthEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
