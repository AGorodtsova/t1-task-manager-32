package ru.t1.gorodtsova.tm.api.client;

import ru.t1.gorodtsova.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {
}
