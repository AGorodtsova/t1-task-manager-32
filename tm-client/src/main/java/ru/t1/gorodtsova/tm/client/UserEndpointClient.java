package ru.t1.gorodtsova.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.client.IUserEndpointClient;
import ru.t1.gorodtsova.tm.dto.request.user.*;
import ru.t1.gorodtsova.tm.dto.response.user.*;

@NoArgsConstructor
public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateProfileUser(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    public UserProfileResponse viewProfileUser(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
