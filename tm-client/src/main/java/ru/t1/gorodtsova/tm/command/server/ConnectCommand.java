package ru.t1.gorodtsova.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.client.IEndpointClient;
import ru.t1.gorodtsova.tm.api.service.IServiceLocator;
import ru.t1.gorodtsova.tm.command.AbstractCommand;
import ru.t1.gorodtsova.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @NotNull
    private final String DESCRIPTION = "Connect to server";

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        try {
            @NotNull final IServiceLocator serviceLocator = getServiceLocator();
            @NotNull final IEndpointClient endpointClient = serviceLocator.getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();

            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getDomainEndpointClient().setSocket(socket);
            serviceLocator.getProjectEndpointClient().setSocket(socket);
            serviceLocator.getSystemEndpointClient().setSocket(socket);
            serviceLocator.getTaskEndpointClient().setSocket(socket);
            serviceLocator.getUserEndpointClient().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
