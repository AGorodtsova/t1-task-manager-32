package ru.t1.gorodtsova.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.endpoint.*;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.repository.IUserRepository;
import ru.t1.gorodtsova.tm.api.service.*;
import ru.t1.gorodtsova.tm.dto.request.domain.*;
import ru.t1.gorodtsova.tm.dto.request.project.*;
import ru.t1.gorodtsova.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.gorodtsova.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.gorodtsova.tm.dto.request.task.*;
import ru.t1.gorodtsova.tm.dto.request.user.*;
import ru.t1.gorodtsova.tm.endpoint.*;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.model.User;
import ru.t1.gorodtsova.tm.repository.ProjectRepository;
import ru.t1.gorodtsova.tm.repository.TaskRepository;
import ru.t1.gorodtsova.tm.repository.UserRepository;
import ru.t1.gorodtsova.tm.service.*;
import ru.t1.gorodtsova.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    public final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ApplicationAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listTaskByProjectId);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskToProject);

        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfileUser);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED));

        taskService.create(test.getId(), "Mega task");
        taskService.create(test.getId(), "Beta task");
    }

    private void prepareShutdown() {
        loggerService.info("** TASK_MANAGER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    public void start() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

}
