package ru.t1.gorodtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.api.service.IServiceLocator;
import ru.t1.gorodtsova.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.gorodtsova.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.gorodtsova.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.gorodtsova.tm.dto.response.system.ApplicationVersionResponse;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
