package ru.t1.gorodtsova.tm.dto.request.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIndexRequest;

@NoArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractIndexRequest {

    public ProjectStartByIndexRequest(@Nullable final Integer index) {
        super(index);
    }

}
