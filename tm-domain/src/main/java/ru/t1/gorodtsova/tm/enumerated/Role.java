package ru.t1.gorodtsova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @Getter
    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
