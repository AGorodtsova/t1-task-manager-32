package ru.t1.gorodtsova.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractUserRequest;
import ru.t1.gorodtsova.tm.enumerated.ProjectSort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort sort;

}
