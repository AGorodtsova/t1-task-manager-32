package ru.t1.gorodtsova.tm.dto.request.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIdRequest;

@NoArgsConstructor
public final class ProjectStartByIdRequest extends AbstractIdRequest {

    public ProjectStartByIdRequest(@Nullable final String id) {
        super(id);
    }

}
