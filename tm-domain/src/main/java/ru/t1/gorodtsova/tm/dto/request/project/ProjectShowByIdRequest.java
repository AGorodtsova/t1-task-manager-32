package ru.t1.gorodtsova.tm.dto.request.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIdRequest;

@NoArgsConstructor
public final class ProjectShowByIdRequest extends AbstractIdRequest {

    public ProjectShowByIdRequest(@Nullable final String id) {
        super(id);
    }

}
